package br.com.refrigeracao.service;

import java.awt.Image;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import javax.imageio.ImageIO;

import org.jrimum.bopepo.BancosSuportados;
import org.jrimum.bopepo.Boleto;
import org.jrimum.domkee.comum.pessoa.endereco.CEP;
import org.jrimum.domkee.comum.pessoa.endereco.Endereco;
import org.jrimum.domkee.comum.pessoa.endereco.UnidadeFederativa;
import org.jrimum.domkee.financeiro.banco.ParametrosBancariosMap;
import org.jrimum.domkee.financeiro.banco.febraban.Agencia;
import org.jrimum.domkee.financeiro.banco.febraban.Carteira;
import org.jrimum.domkee.financeiro.banco.febraban.Cedente;
import org.jrimum.domkee.financeiro.banco.febraban.ContaBancaria;
import org.jrimum.domkee.financeiro.banco.febraban.Modalidade;
import org.jrimum.domkee.financeiro.banco.febraban.NumeroDaConta;
import org.jrimum.domkee.financeiro.banco.febraban.Sacado;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeCobranca;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeTitulo;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo.Aceite;
//msardenberg@bitbucket.org/msardenberg/bm-turismo.git
import br.com.refrigeracao.Resources;


public class BoletoService {

	private static BoletoService INSTANCE = null;

	public static synchronized BoletoService getInstance() {
		if(INSTANCE == null){
			INSTANCE = new BoletoService();
		}
		return  INSTANCE;
	}

	/**
	 * Método que calcula o dígito do nosso numero de acordo com os dados do dono da conta, 
	 * dados do banco, número de controle do boleto interno (nosso numero) e constante de cálculo fornecida pelo o Banco.
	 *  
	 * @param nossoNumero
	 * @return
	 */
	public String geraDvNossoNumero(String nossoNumero){

		//numero cooperativa = 4327;
		//codigo cliente + DV = 245224;
		//constante = 3197;

		String numCoopComCodClient = "43270000245224";

		String auxCalculo = numCoopComCodClient + nossoNumero;
		String constRepetida = "319731973197319731973";

		Integer nr = 0;
		int n1 = 0;
		int n2 = 0;

		for(int j = 0; j < 21; j++){
			n1 = Character.getNumericValue(auxCalculo.charAt(j));  
			n2 = Character.getNumericValue(constRepetida.charAt(j));

			nr = nr + (n1*n2); 
		}
		System.out.println("Cálculo nr = " + nr);
		int resto = nr % 11;

		if(resto == 0 || resto == 1){
			System.out.println("dv = 0");
			return "0";
		}
		Integer onzeMenosResto = 11-resto;

		System.out.println("dv = " + onzeMenosResto);
		return onzeMenosResto.toString();
	}

	public Boleto gerarBoleto(String nomeCliente, String CPForCNPJ, String nossoNumero, Double valorBoleto){

		for(int i = 7 - nossoNumero.length(); i > 0; i--){ // para a base de calculo o Nosso Numero tem que possuir 7 algaritimos 
			nossoNumero = "0" + nossoNumero; 
		}

		/*
		 * INFORMANDO DADOS SOBRE O CEDENTE.
		 */
		Cedente cedente = new Cedente("RAMTEC Serviços de Informática LTDA.", "08.326.385/0001-93");

		/*
		 * INFORMANDO DADOS SOBRE O SACADO.
		 */
		Sacado sacado = new Sacado(nomeCliente, CPForCNPJ);

		// Informando o endereço do sacado.
		Endereco enderecoSac = new Endereco();
		enderecoSac.setUF(UnidadeFederativa.RJ);
		enderecoSac.setLocalidade("Niteroi");
		enderecoSac.setCep(new CEP("24240-070"));
		enderecoSac.setBairro("Santa Rosa");
		enderecoSac.setLogradouro("Rua Itaperuna");
		enderecoSac.setNumero("100");
		sacado.addEndereco(enderecoSac);

		// Informando dados sobre a conta bancária do título.
		ContaBancaria contaBancaria = new ContaBancaria(BancosSuportados.BANCOOB.create());
		contaBancaria.setCarteira(new Carteira(1, TipoDeCobranca.SEM_REGISTRO));
		contaBancaria.setAgencia(new Agencia(4327));
		contaBancaria.setModalidade(new Modalidade(2));
		contaBancaria.setNumeroDaConta(new NumeroDaConta(245224)); // por codigo do cedente e nao conta
		//cedente.addContaBancaria(contaBancaria);
		
		
		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, +7);
		Date vencimento = c.getTime();

		Titulo titulo = new Titulo(contaBancaria, sacado, cedente);
		titulo.setNumeroDoDocumento(nossoNumero); // gerar número para nosso controle interno
		titulo.setNossoNumero(nossoNumero + geraDvNossoNumero(nossoNumero)); // necessário ter 8 dígitos
		//titulo.setDigitoDoNossoNumero(geraDvNossoNumero(nossoNumero)); // calculado
		titulo.setValor(BigDecimal.valueOf(valorBoleto));
		titulo.setDataDoDocumento(date);
		titulo.setDataDoVencimento(vencimento);
		titulo.setTipoDeDocumento(TipoDeTitulo.DM_DUPLICATA_MERCANTIL);
		titulo.setAceite(Aceite.N);;
		titulo.setDesconto(BigDecimal.ZERO);
		titulo.setDeducao(BigDecimal.ZERO);
		titulo.setMora(BigDecimal.ZERO);
		titulo.setAcrecimo(BigDecimal.ZERO);
		titulo.setValorCobrado(BigDecimal.valueOf(valorBoleto));
		
		//titulo.getParametrosBancarios().adicione("ModalidadeDeCobranca", 2);
		titulo.setParametrosBancarios(new ParametrosBancariosMap("ModalidadeDeCobranca", 2));

		/*
		 * INFORMANDO OS DADOS SOBRE O BOLETO.
		 */
		Boleto boleto = new Boleto(titulo);
		boleto.setLocalPagamento("Pagável em qualquer Banco até o vencimento.");
		boleto.setInstrucao1("Não receber após o vencimento");
		boleto.addTextosExtras("txtRsAgenciaCodigoCedente", "4327 / 245224");
		boleto.addTextosExtras("txtFcAgenciaCodigoCedente", "4327 / 245224");
		
		/*
		 * Adaptacoes ao template original (pdf) ao nosso modelo de boleto.
		 */
		Image im;
		try {
			im = ImageIO.read(Resources.getResourceAsStream("template/sicoob.png"));
			boleto.addImagensExtras("txtRsLogoBanco", im);
			boleto.addImagensExtras("txtFcLogoBanco", im);
		} catch (IOException e) {
			System.out.println("Não foi possivel obter a imagem do banco : " + e );
		}
		
		return boleto;
	}


	/**
	 * Exibe o arquivo na tela.
	 * 
	 * @param arquivoBoleto
	 */
	//	private void mostreBoletoNaTela(File arquivoBoleto) {
	//			
	//			java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
	//	
	//			try {
	//				desktop.open(arquivoBoleto);
	//			} catch (IOException e) {
	//				e.printStackTrace();
	//			}
	//}
}
