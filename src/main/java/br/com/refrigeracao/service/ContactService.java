package br.com.refrigeracao.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import br.com.refrigeracao.Resources;
import br.com.refrigeracao.model.Contact;
import br.com.refrigeracao.model.Encryption;

/**
 * 
 * @author Marcelo Sardenberg
 *
 */
public class ContactService implements IAbstractService<Contact, Integer>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	private static ContactService INSTANCE = null;
	private Contact contato;

	public static synchronized ContactService getInstanceObject() {
		if(INSTANCE == null){
			INSTANCE = new ContactService();
		}
		return  INSTANCE;
	}

	public ContactService() {

		try {
			contato = new Contact();
		} catch (IOException e) {
			FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_WARN,"Temporariamente indisponível","") );
			System.out.println("ContactService: " + e);
		}
	}


	/**
	 * Envia email formato HTML de confirmaÃ§Ã£o de recebimento dos dados preenchidos no formulÃ¡srio de contato
	 * 
	 * @param addToName
	 * @param addToEmail
	 * @param subject
	 * @param message
	 * @throws EmailException
	 * @throws IOException
	 */
	public void sendToClient(String addToName, String addToEmail, String subject, String message, String tel, String tipoBusiness, String passageiro, String origem, String destino, String hora, String data) throws IOException, EmailException{  

		InputStream pathFileHtmlStream = Resources.getResourceAsStream("template/emailToClient.html");
		String html = importFileHtml(pathFileHtmlStream, addToName, addToEmail, subject, message, tel, tipoBusiness, passageiro, origem, destino, hora, data);
		String assunto = "Confirmação de recebimento. " + addToName + ", agradecemos o contato." ; 

		configEmail(addToName, addToEmail, assunto, message, html);
	}


	/**
	 * Envia email formato HTML do formulário de contato do site para o email contato@empresa.com
	 * 
	 * @param addToName
	 * @param addToEmail
	 * @param subject
	 * @param message
	 * @throws EmailException
	 * @throws IOException 
	 */
	public void send(String addToName, String addToEmail, String subject, String message, String tel , String tipoBusiness, String passageiro, String origem, String destino, String hora, String data) throws EmailException, IOException {  

		InputStream pathFileHtml = Resources.getResourceAsStream("template/emailForm.html");

		String subjectCompleto = "[Website] " + addToName + " - " + subject;
		String html = importFileHtml(pathFileHtml, addToName, addToEmail, subject, message, tel, tipoBusiness, passageiro, origem, destino, hora, data);

		configEmail(addToName, contato.getFrom(), subjectCompleto, message, html);

	}


	/**
	 * Configuração envio de e-mail.  
	 * 
	 * @param addToName
	 * @param addToEmail
	 * @param subject
	 * @param message
	 * @param smtp
	 * @param port
	 * @param fromEmail
	 * @param password
	 * @param fromName
	 * @param html
	 * @throws EmailException
	 * @throws MalformedURLException
	 */
	public void configEmail(String addToName, String addToEmail, String subject, String message, String html) throws EmailException, MalformedURLException{

		HtmlEmail hEmail = new HtmlEmail();  

		hEmail.setDebug(true);
		hEmail.setHostName(contato.getSmtp());  
		hEmail.setSmtpPort(Integer.parseInt(contato.getPort()));
		hEmail.setAuthentication(contato.getAuthentication(), Encryption.decrypt(contato.getPassword()));  
		hEmail.addTo(addToEmail, addToName);
		hEmail.setFrom(contato.getFrom(), contato.getName());  
		hEmail.setSubject(subject);
		hEmail.addBcc("marcelo.sardenberg@bmturismo.com");
		hEmail.setHtmlMsg(html);  
		hEmail.send();
	}

	/**
	 * Importa arquivo html como string.
	 * @param pathFileHtmlStream
	 * @param addToName
	 * @param addToEmail
	 * @param subject
	 * @param message
	 * @param passageiro 
	 * @param origem 
	 * @param destino 
	 * @param hora 
	 * @param data 
	 * @throws IOException
	 */
	public String importFileHtml(InputStream pathFileHtmlStream,  String addToName, 
			String addToEmail, String subject, String message, String tel, String tipoBusiness, String passageiro, String origem, String destino, String hora, String data) throws IOException{

		InputStreamReader readerHtml = new InputStreamReader(pathFileHtmlStream,"UTF-8");
		BufferedReader brHtml = new BufferedReader(readerHtml);
		String html="";
		String auxHtml = "";

		while((auxHtml = brHtml.readLine()) != null){
			html += auxHtml + "\n";
		}

		html = html.replaceAll("contact.name", addToName);
		html = html.replaceAll("contact.email", addToEmail);
		html = html.replaceAll("contact.subject", subject);
		html = html.replaceAll("contact.message", message);
		html = html.replaceAll("contact.telContato", tel);
		html = html.replaceAll("contact.tipo", tipoBusiness);
		html = html.replaceAll("contact.passenger", passageiro);
		html = html.replaceAll("contact.origem", origem);
		html = html.replaceAll("contact.destino", destino);
		html = html.replaceAll("contact.data", data);
		html = html.replaceAll("contact.hora", hora);
		return html;
	}

	@Override
	public void save() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void edit() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Contact> listAll() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Contact findById() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
}
