package br.com.refrigeracao.service;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Marcelo Sardenberg
 * @since 20140808
 * @param <T>
 * @param <ID>
 */
public interface IAbstractService<T, ID> extends Serializable{
    
    void save()throws Exception;
    void edit()throws Exception;
    void delete()throws Exception;
    List<T> listAll()throws Exception;
    T findById()throws Exception;
    
}
