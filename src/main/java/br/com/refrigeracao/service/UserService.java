package br.com.refrigeracao.service;

import java.util.List;

import br.com.refrigeracao.dao.UserDao;
import br.com.refrigeracao.exeption.AquillaException;
import br.com.refrigeracao.model.User;

public class UserService implements IAbstractService<User, Integer>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private User user; 
	private static UserService INSTANCE = null;
	public static final String VERIFICAR_LOGIN_EXISTENTE = "select login from User where login = :parameter";


	public UserDao getDao(){
		UserDao dao = new UserDao(User.class);
		return dao;
	}
	
	public User getNewUser(){
		user = new User();
		return user;
	}
	
	public static synchronized UserService getInstance(){
		if(INSTANCE == null){
			INSTANCE = new UserService();
		}
		return INSTANCE;
	}

	@Override
	public void save() throws Exception {
		getDao().save(getNewUser());
	}


	@Override
	public void edit() throws Exception {
		getDao().edit(getNewUser());
	}


	@Override
	public void delete() throws Exception {
		getDao().delete(getNewUser());
	}


	@Override
	public List<User> listAll() throws Exception {

		return	getDao().listAll();
	}


	@Override
	public User findById() throws Exception {
		return getDao().findById(getNewUser().getId());
	}

	/**
	 * Verifica se o login informado podera ser utilizado para o cadastro de um
	 * novo usuario
	 * @return boolean
	 * @throws Exception 
	 */
	public boolean authorizesCadastroLogin()throws Exception{

		try{
			return getDao().autorizado(VERIFICAR_LOGIN_EXISTENTE, getNewUser().getLogin());
		}
		catch(Exception e){
			throw new AquillaException("UsuarioService.loginEncontrado(String login) - " + e.toString(), e);
		}
	}
	
	/**
     * Realiza a autenticacao para acesso ao sistema de usuario cadastrados
     * @param login
     * @param senha
     * @return
     * @throws Exception 
     */
    public User autentica(String login, String senha) throws Exception {
        try{ 
            user = getDao().autenticador("login", login, "password", senha);
            return user;
        }
        catch(Exception e){
            throw new AquillaException("UsuarioService.autentica(String login, String senha) - " + e.toString(), e);
       }
    }
    
    public User getUser() {
		return user;
	}
}
