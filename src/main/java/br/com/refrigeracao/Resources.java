package br.com.refrigeracao;

import java.io.InputStream;
import java.net.URL;

public class Resources {

	/**
	 * 
	 * @param url - caminho da pasta ou arquivo que deseja obter a partir do caminho do resource class.
	 * @return a pasta ou arquivo especificado na url
	 */
	public static InputStream getResourceAsStream(String url){
		return Resources.class.getResourceAsStream(url);
	}
	
	/**
	 * @param sulfix
	 * @return a url do arquivo ou pasta 
	 */
	public static URL getURL(String sulfix){
		URL url = Resources.class.getResource("properties/hibernate.cfg.xml");
		return url;
		
	}
	
	public static void main(String[] args) {
		System.out.println(Resources.class.getResource("webapp/restrict/doc/areaRestrita.xhtml"));
	}
}


