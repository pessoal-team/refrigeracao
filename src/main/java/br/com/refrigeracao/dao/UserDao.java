package br.com.refrigeracao.dao;

import br.com.refrigeracao.model.User;

/**
 * 
 * @author Marcelo Sardenberg
 *
 */
public class UserDao extends GenericDao<User> {

	public UserDao(Class<User> clazz) {
		super(clazz);
	}
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

}
