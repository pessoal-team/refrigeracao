package br.com.refrigeracao.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.com.refrigeracao.util.HibernateUtil;

/**
 * Classe gen�rica de acesso a dados que dever� ser extendida por cada classe DAO de suas respecivas entidades
 * @author Marcelo Sardenberg
 * @since 20140810
 *
 * @param <T>
 */
public class GenericDao<T> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final Class<T> classe;
	protected Session session;
	protected Transaction transaction;
	protected Criteria criteria;
	protected Query query;

	public GenericDao(Class<T> classe) {
		session = HibernateUtil.getSessionFactory().openSession();
		this.classe = classe;
	}


	/**
	 * Metodos CRUD Cadastra um novo objeto
	 *
	 * @param t
	 * @throws Exception
	 */
	public void save(T t) throws Exception {
		sessionIsOpen();
		transaction = session.beginTransaction();
		session.save(t);
		transaction.commit();
		session.close();
		System.out.println("daoGenerico: cadastrou");
	}

	/**
	 * Metodos CRUD Edita o objeto desejado
	 *
	 * @param t
	 * @throws Exception
	 */
	public void edit(T t) throws Exception {
		sessionIsOpen();
		transaction = session.beginTransaction();
		session.update(t);
		transaction.commit();
		session.close();
	}

	/**
	 * Metodos CRUD Deleta o objeto desejado
	 *
	 * @param t
	 * @throws Exception
	 */
	public void delete(T t) throws Exception {

		transaction = session.beginTransaction();
		session.delete(t);
		transaction.commit();
		session.close();
	}

	/**
	 * Metodos CRUD Busca objeto pelo id informado
	 *
	 * @param t
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public T findById(Integer id) throws Exception {
		sessionIsOpen();
		return (T) session.get(getClasse(), id);
	}

	/**
	 * Metodos CRUD Retorna uma lista de objetos
	 *
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<T> listAll() throws Exception {
		sessionIsOpen();
		criteria = session.createCriteria(getClasse().getName());
		List<T> lista = (List) criteria.list();
		return lista;
	}

	/**
	 * Lista objetos de acordo com o parametro passado
	 *
	 * @param columnName
	 * @param param
	 * @return List
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<T> listObjectByParam(String columnName, String param) throws Exception {
		sessionIsOpen();
		criteria = session.createCriteria(getClasse().getName())
				.add(Restrictions.eq(columnName, param));
		List<T> lista = (List) criteria.list();
		return lista;
	}

	@SuppressWarnings("unchecked")
	public List<T> listByCriteriaAndId(String column, Integer id) throws Exception {

		sessionIsOpen();
		criteria = session.createCriteria(getClasse().getName(), "o")
				.add(Restrictions.like("o." + column, id));
		List<T> lista = criteria.list();
		return lista;
	}

	/**
	 * Metodo faz uma verificacao para localizar a existencia de um parametro
	 * que ja existe no banco de dados evitando a duplicidade
	 *
	 * @param HQL
	 * @param parameter
	 * @return
	 * @throws Exception
	 */
	public boolean autorizado(String HQL, String parameter) throws Exception {
		sessionIsOpen();
		query = session.createQuery(HQL);
		query.setString("parameter", parameter);
		return query.list().isEmpty() ? true : false;
	}

	/**
	 * Metodo utilizado para realizar uma autenticacao, tal somente sera valdia
	 * se e somente ao passar os dois parametros (param1 e param2) for
	 * localizado um objeto
	 *
	 * @param columnName1
	 * @param param1
	 * @param columnName2
	 * @param param2
	 * @return T
	 * @throws Exception
	 */
	public T autenticador(String columnName1, String param1, String columnName2, String param2) throws Exception {
		sessionIsOpen();

		T obj = null;

		criteria = session.createCriteria(getClasse().getName())
				.add(Restrictions.eq(columnName1.trim(), param1.trim()))
				.add(Restrictions.eq(columnName2.trim(), param2.trim()));

		return obj = (T) criteria.uniqueResult();
	}

	/**
	 * Metoro realiza a busca de um objeto, recebendo uma query e um paramentro
	 *
	 * @param HQL
	 * @param parameter
	 * @return T
	 * @throws Exception
	 */
	public T findObjectByParam(String HQL, String parameter) throws Exception {

		sessionIsOpen();
		query = session.createQuery(HQL).setParameter("parameter", parameter.trim());
		T obj = (T) query.uniqueResult();

		if (obj == null) {
			System.out.println("objeto nulo banco - tag: " + parameter.toString());
		} else {
			System.out.println("carregado");
		}

		return obj;
	}

	public List<T> findObjectByParametroList(String HQL, Object parameter) throws Exception {

		sessionIsOpen();
		query = session.createQuery(HQL).setParameter("parameter", parameter);
		return query.list();
	}

	public Integer findQuantById(String coluna, String param) {

		sessionIsOpen();
		criteria = session.createCriteria(getClass().getName())
				.add(Restrictions.eq(coluna, param))
				.setProjection(Projections.rowCount());
		return (Integer) criteria.uniqueResult();
	}

	/**
	 * Metodo usado apenas para garantir
	 */
	public void sessionIsOpen() {
		if (!session.isOpen() || session == null) {
			session = HibernateUtil.getSessionFactory().openSession();
		}
	}

	public Class<T> getClasse() {
		return this.classe;
	}

}
