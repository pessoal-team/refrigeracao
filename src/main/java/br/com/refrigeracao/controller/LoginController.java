package br.com.refrigeracao.controller;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import br.com.refrigeracao.exeption.AquillaException;
import br.com.refrigeracao.service.UserService;
import br.com.refrigeracao.sessionscoped.UserLogged;
import br.com.refrigeracao.util.UrlsSistema;

/**
 * Contorlador de login.
 * @author Marcelo Sardenberg
 * @since 20140815
 */
@RequestScoped
@ManagedBean
public class LoginController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private UserService service;
	private String login;
	private String pass;

	@ManagedProperty(value = "#{userLogged}")
	private UserLogged usuarioLogado;

	/**
	 * Creates a new instance of LoginControl
	 */
	public LoginController() {
		login = "";
		pass = "";
		service = UserService.getInstance();
	}

	/**
	 * Realiza a autenticacao de acesso logando o usuario em sessao permititndo
	 * acesso ao sistema
	 *
	 * @return String
	 * @throws AquillaException 
	 */
	public String goIn() throws AquillaException {

		String resultado;

		try {
			if (service.autentica(login, pass) != null) {
				usuarioLogado.logging(service.getUser());
				login = "";
				pass = "";
				resultado = UrlsSistema.HOME;
			} else {
				login = "";
				pass = "";
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Login e/ou Senha inv�lidos!", ""));
				resultado = UrlsSistema.LOGIN_NI;
			}

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Temporariamente Indispon�vel", ""));
			resultado = UrlsSistema.LOGIN_NI;
			throw new AquillaException(e.toString());
		}
		return resultado;
	}

	/**
	 * Desloga usuario do sistema
	 *
	 * @return String
	 */
	public String goOut() {

		usuarioLogado.logout();
		return UrlsSistema.LOGIN;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public UserLogged getUsuarioLogado() {
		return usuarioLogado;
	}

	public void setUsuarioLogado(UserLogged usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

}
