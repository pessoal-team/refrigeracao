package br.com.refrigeracao.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.jrimum.bopepo.Boleto;
import org.jrimum.bopepo.view.BoletoViewer;

import br.com.refrigeracao.service.BoletoService;

@RequestScoped
@ManagedBean
public class BoletoController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nomeCliente;
	private String cpfCnpj;
	private String mask;
	private Integer nossoNumero;
	private Double valorBoleto;
	private Integer quantBoletos;
	private boolean cpfOrCnpjBoolean;


	public void configBoleto(){	
		
		Boleto boleto;
		
//		if(this.quantBoletos == null || this.quantBoletos == 0){
//			this.quantBoletos = 1;
//		}
//		while(this.quantBoletos > 0){
			boleto = BoletoService.getInstance().gerarBoleto(nomeCliente, cpfCnpj, nossoNumero.toString(), valorBoleto);
			System.out.println("Configuração do Boleto feita...");
//			nossoNumero++;
//			this.quantBoletos--;
//		}
		HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
		byte[] pdfByte  = BoletoViewer.create(boleto).getPdfAsByteArray();

		try {
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename=Boleto - " + nomeCliente + ".pdf");

			OutputStream output = response.getOutputStream();
			output.write(pdfByte);
			response.flushBuffer();
			FacesContext.getCurrentInstance().responseComplete();

		} catch (IOException e) {
			e.printStackTrace();
		}

		nomeCliente ="";
		cpfCnpj="";
		nossoNumero=null;
		valorBoleto=null;
		System.out.println("fim metodo config boleto...");
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public Integer getNossoNumero() {
		return nossoNumero;
	}
	
	public void setNossoNumero(Integer nossoNumero) {
		this.nossoNumero = nossoNumero;
	}

	public Double getValorBoleto() {
		return valorBoleto;
	}

	public void setValorBoleto(Double valorBoleto) {
		this.valorBoleto = valorBoleto;
	}

	public Integer getQuantBoletos() {
		return quantBoletos;
	}

	public void setQuantBoletos(Integer quantBoletos) {
		this.quantBoletos = quantBoletos;
	}

	public boolean isCpfOrCnpjBoolean() {
		return cpfOrCnpjBoolean;
	}

	public void setCpfOrCnpjBoolean(boolean cpfOrCnpjBoolean) {
		this.cpfOrCnpjBoolean = cpfOrCnpjBoolean;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getMask() {
		return mask = cpfOrCnpjBoolean == false ? "99.999.999/0001-99" : "999.999.999-99";
	}

	public void setMask(String mask) {
		this.mask = mask;
	}

}		
