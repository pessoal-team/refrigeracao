package br.com.refrigeracao.controller;

import java.io.Serializable;
import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@SessionScoped
@ManagedBean
public class I18nController implements Serializable {

	private static final long serialVersionUID = 1L;

	private final Locale ENGLISH = Locale.ENGLISH;
	private final Locale PORTUGUES = new Locale("pt");
	private Locale locale = PORTUGUES;
	
	public Locale getLocale() {
		return (locale);
	}

	public void setEnglish() {
		locale = ENGLISH;
		updateViewLocale();
	}

	public void setPortugues() {
		locale = PORTUGUES;
		updateViewLocale();
	}

	private void updateViewLocale() {
		FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
	}

}
