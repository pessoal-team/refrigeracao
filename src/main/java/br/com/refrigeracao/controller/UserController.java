package br.com.refrigeracao.controller;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import br.com.refrigeracao.model.User;
import br.com.refrigeracao.service.UserService;
import br.com.refrigeracao.sessionscoped.UserLogged;

@ManagedBean
@RequestScoped
public class UserController {

	@ManagedProperty(value = "#{usuarioLogado}")
	private UserLogged userLogged;

	private UserService service;
	private List<User> users;
	private List<User> filtroUser;

	public UserController() {
		service = UserService.getInstance();
	}

	/**
	 * Verifica se o Login desejado ja existe
	 */
	public void verificaLogin() {
		try {
			if (!UserService.getInstance().authorizesCadastroLogin()) {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Login informado j� existem! Informe outro.", ""));
			}

		} catch (Exception e) {
			System.out.println(e);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Temporariamente Indispon�vel", ""));
		}
	}

	/**
	 * Cadastra e logo o usuario no sistema
	 *
	 * @return String
	 */
	public void register() {

		try {
			service.save();
			userLogged.logging(service.getNewUser());
			service.getNewUser();
			FacesContext.getCurrentInstance().addMessage("form", new FacesMessage(FacesMessage.SEVERITY_INFO, "Usu�rio Cadastrado com sucesso!", ""));
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage("form", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Temporariamente Indispon�vel", ""));
		}
	}

}
