package br.com.refrigeracao.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.refrigeracao.service.ContactService;

/**
 *
 * @author Marcelo Sardenberg
 * @since 20140624
 */
@ViewScoped
@ManagedBean
public class ContactController implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    Logger logger = LoggerFactory.getLogger(ContactController.class);

    private String name;
    private String addTo;
    private String subject;
    private String message;
    private String telContato;
    private String typeBusiness = "";
    private Date dataEmbarque;
    private String horaEmbarque = "";
    private String passenger = "";
    private String origem = "";
    private String destination = "";
    private boolean orcamentoBoolean;

    public void send() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().setKeepMessages(true);
        String data = "";

        if (orcamentoBoolean || dataEmbarque != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            data = sdf.format(dataEmbarque);
        }

        try {
            ContactService contatoService = ContactService.getInstanceObject();
            contatoService.sendToClient(name, addTo, subject, message, telContato, typeBusiness, passenger, origem, destination, horaEmbarque, data);
            contatoService.send(name, addTo, subject, message, telContato, typeBusiness, passenger, origem, destination, horaEmbarque, data);
            context.addMessage(null, new FacesMessage("E-mail enviado com sucesso."));
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Temporariamente indisponível", ""));
            System.out.println("SEND FAIL: " + e);
            logger.error("SEND FAIL: ", e.getMessage());
        }
        name = "";
        addTo = "";
        subject = "";
        message = "";
        telContato = "";
        typeBusiness = "";
        destination = "";
        horaEmbarque = "";
        passenger = "";
        origem = "";
    }

    public void orcamentoSet() {
        System.out.println("............. true");
        this.orcamentoBoolean = true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddTo() {
        return addTo;
    }

    public void setAddTo(String addTo) {
        this.addTo = addTo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTelContato() {
        return telContato;
    }

    public void setTelContato(String telContato) {
        this.telContato = telContato;
    }

    public boolean isOrcamentoBoolean() {
        return orcamentoBoolean;
    }

    public void setOrcamentoBoolean(boolean orcamentoBoolean) {
        this.orcamentoBoolean = orcamentoBoolean;
    }

    public Logger getLogger() {
        return logger;
    }

    public String getTypeBusiness() {
        return typeBusiness;
    }

    public String getHoraEmbarque() {
        return horaEmbarque;
    }

    public String getPassenger() {
        return passenger;
    }

    public String getOrigem() {
        return origem;
    }

    public String getDestination() {
        return destination;
    }

    public void setTypeBusiness(String typeBusiness) {
        this.typeBusiness = typeBusiness;
    }

    public void setHoraEmbarque(String horaEmbarque) {
        this.horaEmbarque = horaEmbarque;
    }

    public void setPassenger(String passenger) {
        this.passenger = passenger;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getDataEmbarque() {
        return dataEmbarque;
    }

    public void setDataEmbarque(Date dataEmbarque) {
        this.dataEmbarque = dataEmbarque;
    }
}
