package br.com.refrigeracao.controller;

import java.io.InputStream;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import br.com.refrigeracao.Resources;

@RequestScoped
@ManagedBean
public class DownloadController {

	private StreamedContent file;
	
	FacesContext context = FacesContext.getCurrentInstance();
	HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();

	public DownloadController() { 
		download();
	}

	public void download() {

		try {
			String nameFile = request.getParameter("nameFile");
			String pathFile = request.getParameter("pathFile");
			
			InputStream pathFilePdf = Resources.getResourceAsStream("download/" + pathFile);
			
			file = new DefaultStreamedContent(pathFilePdf, "application/pdf",nameFile );
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,"Temporariamente Indispon�vel", ""));
			e.printStackTrace();
		}
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}
}
