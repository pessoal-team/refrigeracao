package br.com.refrigeracao.sessionscoped;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import br.com.refrigeracao.model.User;

@SessionScoped
@ManagedBean
public class UserLogged implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private User user;
	
	private String nome = "Marcelo";
	
	public UserLogged() {
	}

	public void logging(User user){
		this.user = user;
	}

	public void logout(){
		this.user = null;
	}

	public boolean isLogged(){
		return this.user != null;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public String getNome() {
		return nome;
	}
}
