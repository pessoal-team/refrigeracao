package br.com.refrigeracao.exeption;

import java.io.Serializable;

import org.apache.log4j.Logger;


/**
 * Tratamento de exce��o b�sica.
 * 
 * @author Marcelo Sardenberg
 * @since 20140808
 *
 */
public class AquillaException extends Exception implements Serializable {

	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public static final Logger LOGGER = Logger.getLogger(AquillaException.class);
     

	    public AquillaException(String message) {
	        super(message);
	        LOGGER.error(message);
	    }

	    public AquillaException(Throwable cause) {
	        super(cause);
	        LOGGER.error(cause);
	    }

	    public AquillaException(String message, Throwable cause) {
	        super(message, cause);
	        LOGGER.error(message, cause);
	    }
	    
}
