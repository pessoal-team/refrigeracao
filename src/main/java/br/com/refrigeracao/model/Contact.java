package br.com.refrigeracao.model;

import java.io.IOException;
import java.util.Properties;

import br.com.refrigeracao.Resources;


/**
 * 
 * @author Marcelo Sardenberg
 * @since 20140624
 */
public class Contact {

	private String smtp;
	private String from;
	private String authentication;
	private String password;
	private String name;
	private String port;
	private boolean tlsEnable;


	public Contact() throws IOException {

		Properties properties = new Properties();
		properties.load(Resources.getResourceAsStream("properties/email.properties"));

		this.smtp = properties.getProperty("prop.email.smtp");
		this.from = properties.getProperty("prop.email.from");
		this.authentication = properties.getProperty("prop.email.authentication");
		this.password = properties.getProperty("prop.email.password");
		this.name = properties.getProperty("prop.email.name");
		this.port = properties.getProperty("prop.email.port");
//		this.tlsEnable = Boolean.parseBoolean(properties.getProperty("prop.email.tsl.enable"));
	}


	public String getSmtp() {
		return smtp;
	}

	public String getFrom() {
		return from;
	}


	public String getPassword() {
		return password;
	}


	public String getName() {
		return name;
	}


	public String getPort() {
		return port;
	}

	public String getAuthentication() {
		return authentication;
	}


	public boolean isTlsEnable() {
		return tlsEnable;
	}

}
