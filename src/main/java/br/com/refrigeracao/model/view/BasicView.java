package br.com.refrigeracao.model.view;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

@ManagedBean
@ViewScoped
public class BasicView implements Serializable {
     
	private static final long serialVersionUID = 6685125616995592850L;
	
	private TreeNode root;
	private String styleClass;
	private int count = 0;
	
	private String checkedString = "fsds";

	private Boolean checked = true;
	
    @PostConstruct
    public void init() {
        root = new DefaultTreeNode("Root", null);
        //node Fi A��es
        TreeNode fia = new DefaultTreeNode("path","FI A��es", root);
        new DefaultTreeNode("item","Small Caps FIA", fia);
        
        //node FI Cotas
        TreeNode fic = new DefaultTreeNode("path","FI Cotas", root);
        new DefaultTreeNode("item", "Northview FIC FIM", fic);
        
        //node FI Imobili�rio
        TreeNode fii = new DefaultTreeNode("path","FI Imobili�rio", root);
        new DefaultTreeNode("item","Aquilla FII", fii);
        new DefaultTreeNode("item","�quilla Renda FII", fii);
        new DefaultTreeNode("item","Firenze FI", fii);
        new DefaultTreeNode("item","Texas FII", fii);
        
        //node FI Multimercado
        TreeNode fim = new DefaultTreeNode("path","FI Multimercado", root);
        new DefaultTreeNode("item","Aquilla GS Absoluto", fim);
        new DefaultTreeNode("item","Aquilla GS Din�mico", fim);
        new DefaultTreeNode("item","Aquilla GS Hedge", fim);
        new DefaultTreeNode("item","Aquilla Veyron FIM IE", fim);
        
        //node FI Participa�oes
        TreeNode fip = new DefaultTreeNode("path", "FI Participa��es", root);
        new DefaultTreeNode("item", "Conquest", fip);
        new DefaultTreeNode("item", "Aivit�", fip);
        new DefaultTreeNode("item", "Multisetorial Sudeste", fip);
        new DefaultTreeNode("item", "Northview", fip);
        new DefaultTreeNode("item", "Veneza", fip);
        new DefaultTreeNode("item", "Vit�ria", fip);
        
    }
 
    public TreeNode getRoot() {
        return root;
    }
    
    public String getStyleClassPath(){
		return "funds_menu_left_menupath";
	}
    public String getStyleClassItem(){
		return "funds_menu_left_menuitem";
	}
    
    
    public void changeCheckbox(){
    	this.checked = true;
    }
    
    public String getCheckedString(){
    	if(checked){
    		return "checked=\"checked\"";
    	}else{
    		return "";
    	}
    }
}