package br.com.refrigeracao.model;

import org.jasypt.util.text.BasicTextEncryptor;


public abstract class Encryption {
	
	
	public static String decrypt(String senha){
		BasicTextEncryptor basicTextEncryptor = new BasicTextEncryptor();
		basicTextEncryptor.setPassword("UTF-8");
		String pass = basicTextEncryptor.decrypt(senha);
		return pass;
	}
	
	public static void main(String[] args) {
        String seuTexto = "bmturismo@contato";
        System.out.println("Texto sem criptografia: " + seuTexto);

        BasicTextEncryptor basicTextEncryptor = new BasicTextEncryptor();

        basicTextEncryptor.setPassword("UTF-8");
        String seuTextoCriptografado = basicTextEncryptor.encrypt(seuTexto);
        String seuTextoNovamenteDescriptografado = basicTextEncryptor.decrypt(seuTextoCriptografado);

        System.out.println("Criptografado: " + seuTextoCriptografado);
        System.out.println("Descriptografado: " + seuTextoNovamenteDescriptografado);

    }
	
}
