package br.com.refrigeracao.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import br.com.refrigeracao.Resources;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author Marcelo Sardenberg
 * @since 20140808
 * 
 */
public class HibernateUtil {

    private static final SessionFactory sessionFactory;
    
    static {
    	
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
            sessionFactory = new Configuration().configure(Resources.getURL("/properties/hibernate.cfg.xml")).buildSessionFactory();
        } catch (Throwable ex) {
            // Log the exception. 
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}