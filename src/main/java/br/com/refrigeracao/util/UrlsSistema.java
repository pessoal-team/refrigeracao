package br.com.refrigeracao.util;

public interface UrlsSistema {
	
	//LOGIN
    public static final String LOGIN = "/areaRestrita.xhtml?faces-redirect=true";
    public static final String LOGIN_NI = "/areaRestrita.xhtml";
    
    //HOME
    public static final String HOME = "/restrict/view/system/home.xhtml?faces-redirect=true";
    public static final String HOME_NI = "/restrict/view/system/home.xhtml";
	
	
}
